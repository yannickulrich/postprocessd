#!/bin/sh

BINARY="cmake-build-debug/postprocessd"

rm -rf testresult
mkdir -p testresult

for shot in testset/*/ ; do
  name="$(basename $shot)"
  $BINARY "testset/$name" "testresult/$name" 1
done