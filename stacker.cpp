#include <stdlib.h>
#include "stacker.h"
#include "stackercpp.h"

stacker_t *
stacker_create(int verbose)
{
    stacker_t *st;
    Stacker *obj;

    st = (__typeof__(st)) malloc(sizeof(*st));
    obj = new Stacker(verbose != 0);
    st->obj = obj;
    return st;
}

void
stacker_add_image(stacker_t *st, unsigned char *data, int width, int height)
{
    Stacker *obj;
    if (st == NULL) {
        return;
    }

    obj = static_cast<Stacker * >(st->obj);
    obj->add_frame(data, width, height);
}

char *
stacker_get_result(stacker_t *st)
{
    Stacker *obj;
    if (st == NULL) {
        return NULL;
    }
    obj = static_cast<Stacker * >(st->obj);
    return obj->get_result();
}

char *
stacker_postprocess(stacker_t *st, unsigned char *data, int width, int height)
{
    Stacker *obj;
    if (st == NULL) {
        return NULL;
    }
    obj = static_cast<Stacker * >(st->obj);
    return obj->postprocess(data, width, height);
}

int
stacker_get_width(stacker_t *st)
{
    Stacker *obj;
    obj = static_cast<Stacker * >(st->obj);
    return obj->get_width();
}

int
stacker_get_height(stacker_t *st)
{
    Stacker *obj;
    obj = static_cast<Stacker * >(st->obj);
    return obj->get_height();
}